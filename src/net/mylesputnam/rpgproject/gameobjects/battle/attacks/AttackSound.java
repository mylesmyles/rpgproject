package net.mylesputnam.rpgproject.gameobjects.battle.attacks;

public abstract class AttackSound
{
	public abstract void playAttackSound();
}
