package net.mylesputnam.rpgproject.gameobjects.person.actor;

import net.mylesputnam.rpgproject.gameobjects.GameMap;
import net.mylesputnam.rpgproject.gameobjects.TextureManager;
import net.mylesputnam.rpgproject.input.InputHandler;

import com.badlogic.gdx.math.Vector2;

public class PlayerActor extends Actor
{
	private InputHandler inputHandler;
	
	public PlayerActor(Vector2 position, GameMap gameMap)
	{
		super(position, gameMap);
		this.inputHandler = InputHandler.getInstance();
		this.texture = TextureManager.get().getPlayerTexture();
	}

	@Override
	public void update(float delta)
	{
		//will do nothing if we don't have target
		moveTowardsTarget(delta);
		//will do nothing if we DO have target
		moveInDirection(inputHandler.getCurrentDirection());
	}
}