package net.mylesputnam.rpgproject.gameobjects.collectible;

import net.mylesputnam.rpgproject.gameobjects.displaymessage.DisplayMessage;
import net.mylesputnam.rpgproject.gameobjects.person.Player;
import net.mylesputnam.rpgproject.view.StatsRenderer;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class MpIncrease extends Collectible
{
	private float mpIncreaseAmount;
	
	public MpIncrease(TextureRegion texture, Vector2 position, float mpIncreaseAmount)
	{
		super(texture, position);
		
		this.mpIncreaseAmount = mpIncreaseAmount;
	}

	@Override
	public void execute(Player player)
	{
		player.getStats().changeMp(mpIncreaseAmount);
		DisplayMessage message = new DisplayMessage("             Mana increased by " + Float.toString(mpIncreaseAmount), 2f);
		StatsRenderer.get().addMessage(message);
		//play sound here
	}

}